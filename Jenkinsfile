def APP_NAME
def APP_NAMESPACE
def APP_VERSION
def IMAGE_TAG
def CURRENT_VERSION
def NEXT_VERSION

podTemplate(inheritFrom: 'default',
    containers: [
        containerTemplate(name: 'kubectl', image: 'bitnami/kubectl:1.27.8', command: 'sleep', args: '99d', runAsUser: '1000', runAsGroup: '1000'),
        containerTemplate(name: 'helm', image: 'alpine/helm:3.15.1', command: 'sleep', args: '99d'),
        containerTemplate(name: 'kaniko', image: 'gcr.io/kaniko-project/executor:debug', command: 'sleep', args: '99d'),
        containerTemplate(name: 'trivy', image: 'aquasec/trivy:latest', command: 'sleep', args: '99d'),
        containerTemplate(name: 'vault', image: 'hashicorp/vault:1.15', command: 'sleep', args: '99d'),
    ],
    volumes: [
        //configMapVolume(configMapName: 'cm-harbor-creds', mountPath: '/kaniko/.docker/config.json', subPath: 'config.json'),
        secretVolume(secretName: 'regcreds', mountPath: '/kaniko/.docker'),
        hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock')
    ]
)
{
    node(POD_LABEL){
        stage('Obtención código fuente') {
            checkout scm

            script {
                APP_NAME = 'ecom-website'
                APP_NAMESPACE = 'ecom'
                APP_VERSION = sh (
                    script: 'cat app/version.txt',
                    returnStdout: true
                ).trim()

                if (env.BRANCH_NAME == 'develop') {
                    IMAGE_TAG="${APP_VERSION}-dev"
                } else {
                    IMAGE_TAG="${APP_VERSION}"
                }
            }
        }
        
        stage('Compilación') {
            echo "📦 Compilando código fuente"
            echo "Built ${APP_NAME} ${IMAGE_TAG}"
        }
        
        stage('Tests unitarios') {
            echo "✅  Unit Testing"
        }
        
        stage('Sonarqube') {
            echo "🔎 Escaneando con Sonarqube"
        }
        
        stage('Construcción imagen') {
            environment {
                PATH = "/busybox:/kaniko:$PATH"
            }
            
            echo "📦 Construyendo y publicando imagen"

            dir('app') {
                container(name: 'kaniko', shell: '/busybox/sh') {
                    sh """#!/busybox/sh
                    /kaniko/executor --dockerfile Dockerfile \
                        --ignore-path=/busybox \
                        --context dir:///`pwd` \
                        --destination harbor.rancher-arsys-demo.hoplasoftware.com/library/${APP_NAME}:${IMAGE_TAG} \
                        --destination harbor.rancher-arsys-demo.hoplasoftware.com/library/${APP_NAME}:latest
                    """
                        //--destination docker.io/hoplasoftware/${APP_NAME}:${IMAGE_TAG}
                        //--destination docker.io/hoplasoftware/${APP_NAME}:latest
                    //"""
                }
            }
        }
        
        stage('Escaneo vulnerabilidades') {
            echo "⭕ Escaneando vulnerabilidades"

            script {
                withCredentials([usernameColonPassword(credentialsId: 'harbor-registry', variable: 'HARBOR_CREDS')]) {
                    sh """
                        curl -X POST -u "${HARBOR_CREDS}" https://harbor.rancher-arsys-demo.hoplasoftware.com/api/v2.0/projects/library/repositories/${APP_NAME}/artifacts/${IMAGE_TAG}/scan
                    """
                }
            }
            
            container('trivy') {
                sh "mkdir reports"

                sh "trivy image \
                --no-progress \
                --vuln-type os \
                --severity HIGH,CRITICAL \
                --format template --template '@/contrib/html.tpl' \
                -o reports/${APP_NAME}_${IMAGE_TAG}.html \
                harbor.rancher-arsys-demo.hoplasoftware.com/library/${APP_NAME}:${IMAGE_TAG}"
              
                publishHTML target : [
                    allowMissing: true,
                    alwaysLinkToLastBuild: true,
                    keepAll: true,
                    reportDir: 'reports',
                    reportFiles: "${APP_NAME}_${IMAGE_TAG}.html",
                    reportName: 'Vulnerability Scan',
                    reportTitles: 'Vulnerability Scan'
              ]
            }
        }
        
        stage('Publicación Helm Chart') {
            echo "🔁📦 Actualizando Helm Chart"

            dir("charts/${APP_NAME}") {
                script {
                    CURRENT_VERSION = sh (
                        script: "cat Chart.yaml | sed -nre 's/version: ([0-9]+\\.[0-9]+\\.[0-9]+)/\\1/p'",
                        returnStdout: true
                    ).trim()
                    
                    if (env.BRANCH_NAME == 'develop') {
                        NEXT_VERSION = sh (
                            script: "echo ${CURRENT_VERSION} | awk -F '.' -v OFS=. '{\$3++;print}\'",
                            returnStdout: true
                        ).trim()
                    } else {
                        NEXT_VERSION = sh (
                            script: "echo ${CURRENT_VERSION} | awk -F '.' -v OFS=. '{\$2++;\$3=0;print}\'",
                            returnStdout: true
                        ).trim()
                    }
                    
                    echo "🔼 Subiendo versión del Chart a ${NEXT_VERSION}"

                    sh "sed -i -E \"s/^version: [0-9]+\\.[0-9]+\\.[0-9]+/version: ${NEXT_VERSION}/g\" Chart.yaml"
                    sh "sed -i -E \"s/^appVersion: .*\$/appVersion: ${IMAGE_TAG}/g\" Chart.yaml"
                    sh "sed -i -E \"s/tag: .*\$/tag: ${IMAGE_TAG}/g\" values.yaml"
                }
            }

            container('helm') {
                sh "helm dependency --debug build charts/${APP_NAME}"
                sh "helm package --debug charts/${APP_NAME}"

                echo "🚀📦 Publicando Helm Chart"

                withCredentials([usernamePassword(credentialsId: 'harbor-registry', passwordVariable: 'HARBOR_PASS', usernameVariable: 'HARBOR_USER')]) {
                    sh "helm registry login -u ${HARBOR_USER} -p ${HARBOR_PASS} harbor.rancher-arsys-demo.hoplasoftware.com"
                    sh "helm push ${APP_NAME}-${NEXT_VERSION}.tgz oci://harbor.rancher-arsys-demo.hoplasoftware.com/helm"
                }
            }
            
            dir("charts/${APP_NAME}") {
                withCredentials([gitUsernamePassword(credentialsId: 'gitlab_dvelascoa', gitToolName: 'Default')]) {
                    sh """
                        git config user.email "jenkins@rancher-arsys-demo.hoplasoftware.com"
                        git config user.name "Jenkins"
                        git add Chart.yaml
                        git add values.yaml
                        git commit -m \"[${APP_NAME}] Release chart ${NEXT_VERSION}\"
                        git push origin HEAD:step_4
                    """
                }
            }
        }
        
        stage('Desplegando App') {
            container('kubectl') {
                script {
                    def is_db_installed = sh(script: "kubectl get applications.argoproj.io --no-headers -o name -n argocd | grep -e ecom-database | wc -l", returnStdout: true)
                    if (is_db_installed == 0) {
                        echo "🐙 Desplegando base de datos con ArgoCD"
                        sh(script: "kubectl apply -n argocd -f argoapp-ecom-database.yaml", returnStdout: true)
                        
                        echo "Esperando a que esté disponible la DB"
                        sleep 30
                        sh(script: "kubectl wait --for=condition=Ready --timeout=180s pod/ecom-database-mariadb-0 -n ${APP_NAMESPACE}", returnStdout: true)
                    } else {
                        echo "🧾 La base de datos ya estaba previamente desplegada"
                    }

                }
            }
            
            container('vault') {
                try {
                    script {
                        def is_db_installed = sh(script: "kubectl get applications.argoproj.io --no-headers -o name -n argocd | grep -e ecom-database | wc -l", returnStdout: true)
                        
                        if (is_db_installed == 0) {
                            echo "🔒 Configurando Vault"

                            withCredentials([[$class: 'VaultTokenCredentialBinding', 
                                addrVariable: 'VAULT_ADDR', 
                                credentialsId: 'jenkins-approle', 
                                tokenVariable: 'VAULT_TOKEN', 
                                vaultAddr: 'http://vault.vault.svc.cluster.local:8200']]) {

                                // Creamos configuración de conexión con db y rol
                                echo "Configurando base de datos"
                                sh(script: """
                                    vault write database/config/ecom-database \
                                    plugin_name=mysql-database-plugin \
                                    connection_url="{{username}}:{{password}}@tcp(ecom-database-mariadb.ecom.svc.cluster.local:3306)/" \
                                    allowed_roles="ecom-website" \
                                    username="vault" \
                                    password="SxeHqBs7"
                                """, returnStdout: true)

                                echo "Rotando credencial"
                                sh(script: "vault write -force database/rotate-root/ecom-database", returnStdout: true)

                                echo "Configurando rol de base de datos"
                                sh(script: """
                                    vault write database/roles/ecom-website \
                                    db_name=ecom-database \
                                    creation_statements="CREATE USER '{{name}}'@'%' IDENTIFIED BY '{{password}}';GRANT SELECT ON ecomdb.* TO '{{name}}'@'%';" \
                                    default_ttl="8h" \
                                    max_ttl="72h"
                                """, returnStdout: true)
                            }
                        }
                    }
                } catch (Exception e) {
                    echo "❌ ERROR"
                    echo 'Exception occurred: ' + e.toString()
                }                
            }
            
            container('kubectl') {
                echo "🐙 Desplegando aplicación con ArgoCD"
                sh(script: "kubectl apply -f argoapp-ecom-website.yaml -n argocd", returnStdout: true)
            }
        }
    }
}
