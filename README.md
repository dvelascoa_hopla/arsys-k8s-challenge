# Arsys Kubernetes Challenge

Este aplicativo es un ejemplo de un e-commerce y es un fork del siguiente repositorio: 

https://github.com/kodekloudhub/learning-app-ecommerce

## Descripción

Hemos distribuido los pasos del reto en distintas ramas de Git.

Para trabajar, utilizaremos el terminal de Rancher donde descargaremos el código desde Git y aplicaremos con el comando kubectl.

